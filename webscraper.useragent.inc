<?php

/**
 * Implements hook_form().
 */
function webscraper_useragent_list_form($form, &$form_state) {
  $form = array();

  $user_agents = _webscraper_get_user_agent_groups_with_strings();

  $header = array(
    'id' =>  t('ID'),
    'string' => t('String')
  );

  foreach ($user_agents as $user_agent) {
    $form['group_' . $user_agent['group_id']] = array(
      '#type' => 'fieldset',
      '#title' => $user_agent['group_name'],
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $data = array();
    foreach ($user_agent['strings'] as $id => $string) {
      $data[$id] = array(
        "id" => $id,
        "string" => $string
      );
    }

    $form['group_' . $user_agent['group_id']]['strings_' . $user_agent['group_id']] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $data,
      '#multiple' => TRUE,
      '#empty' => t('No user agents in this group.')
    );

    if (count($data) > 0) {
      $form['group_' . $user_agent['group_id']]['actions']['delete'] = array(
        '#type' => 'submit',
        '#name' => 'delete_' . $user_agent['group_id'],
        '#value' => t('Delete selected'),
        '#attributes' => array('class' => array('delete-user-agent'))
      );
    }

    $form['group_' . $user_agent['group_id']]['actions']['deletegroup'] = array(
      '#type' => 'submit',
      '#value' => t('Delete group'),
      '#name' => 'deletegroup_' . $user_agent['group_id']
    );
  }

  return $form;
}

function webscraper_useragent_list_form_submit($form, &$form_state) {
  $button_name = $form_state['triggering_element']['#name'];
  $button_name = explode('_', $button_name);

  switch ($button_name[0]) {

    // Handle deletion of user agents
    case "delete":
      $deleted = FALSE;
      $group_id = $button_name[1];
      foreach ($form_state['values']['strings_' . $group_id] as $id => $selected) {
        if ($selected != 0) {
          db_delete('webscraper_user_agent')->condition('id', $id)->execute();
          $deleted = TRUE;
        }
      }
      if ($deleted === TRUE) {
        drupal_set_message(t('The selected user agents were deleted successfully.'));
      }
    break;

    // Handle deletion of group
    case "deletegroup":
      $group_id = $button_name[1];
      db_delete('webscraper_user_agent')->condition('group_id', $group_id)->execute();
      db_delete('webscraper_user_agent_group')->condition('id', $group_id)->execute();
      drupal_set_message(t('The user agent group and its strings have been deleted.'));
    break;

  }
}

/**
 * Implements hook_form().
 */
function webscraper_useragent_add_form($form, &$form_state) {
  $form = array();

  $form['group'] = array(
    '#type' => 'select',
    '#title' => t('Group'),
    '#description' => t('Choose which group these user agents should be added to.'),
    '#options' => _webscraper_get_user_agent_groups()
  );

  $form['new_user_agents'] = array(
    '#type' => 'textarea',
    '#title' => t('Add new user agents to this group'),
    '#description' => t('Use this form to add new user agents to this group. One per line.')
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add user agent(s)')
  );

  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('webscraper_useragent_add_form_cancel'),
    '#limit_validation_errors' => array()
  );

  return $form;
}

function webscraper_useragent_add_form_submit($form, &$form_state) {
  $agents = explode("\n", $form_state['values']['new_user_agents']);
  foreach ($agents as $agent) {
    if ($agent != "") {
      db_insert('webscraper_user_agent')->fields(array('group_id' => $form_state['values']['group'], 'string' => trim($agent)))->execute();
    }
  }
  drupal_set_message(t('The user agents have been added.'));
  drupal_goto('admin/config/services/webscraper/useragent');
}

function webscraper_useragent_add_form_cancel($form, &$form_state) {
  $url = isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/services/webscraper/useragent';
  drupal_goto($url);
}

/**
 * Implements hook_form().
 */
function webscraper_useragent_group_add_form($form, &$form_state) {
  $form = array();

  $form['new_group'] = array(
    '#type' => 'textfield',
    '#title' => t('Add a new group'),
    '#description' => t('Type the name which you want the new group to have.'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add new group')
  );

  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('webscraper_useragent_add_form_cancel'),
    '#limit_validation_errors' => array()
  );

  return $form;
}

function webscraper_useragent_group_add_form_submit($form, &$form_state) {
  db_insert('webscraper_user_agent_group')->fields(array('name' => trim($form_state['values']['new_group'])))->execute();
  drupal_set_message(t('The new group has been created.'));
  drupal_goto('admin/config/services/webscraper/useragent');
}
