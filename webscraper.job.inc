<?php



/**
 * Implements hook_form().
 */
function webscraper_job_list_form($form, &$form_state) {
  $form = array();

  // Scraping jobs
  $form['webscraper_admin_form_scraping_jobs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Scraping jobs'),
    '#description' => t('Manage your scraping jobs.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['webscraper_admin_form_scraping_jobs']['addjob'] = array(
    '#type' => 'submit',
    '#value' => t('Add new job'),
    '#name' => 'addjob'
  );

  return $form;
}


function webscraper_admin_form_submit($form, &$form_state) {

  $button_name = $form_state['triggering_element']['#name'];
  $button_name = explode('_', $button_name);

  switch ($button_name[0]) {

    // Handle adding of new job (redirect to creation page)
    case "addjob":
      drupal_goto('admin/config/services/webscraper/job/new');
    break;

  }
}

function webscraper_job_add_form_cancel($form, $form_state) {
  drupal_goto('admin/config/services/webscraper/job');
}

/**
 * Implements hook_form().
 */
function webscraper_job_add_form_page_1($form, &$form_state) {
  $form = array();

  // Make sure the user is on the correct page
  if (!empty($form_state['page_num']) && $form_state['page_num'] != 1) {
    return call_user_func('webscraper_job_add_form_page_' . $form_state['page_num'], $form, $form_state);
  }

  $form_state['page_num'] = 1;

  $form['step'] = array(
    '#markup' => '<h2>' . t('Step 1 of 4: General settings') . '</h2>'
  );

  $form['user_agent_group'] = array(
    '#type' => 'select',
    '#title' => t('User agent'),
    '#description' => t('Choose from which user agent group a user agent string should be randomly selected from (which will be used when fetching data).'),
    '#options' => _webscraper_get_user_agent_groups(),
    '#default_value' => !empty($form_state['values']['user_agent_group']) ? $form_state['values']['user_agent_group'] : '',
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The URL from where the data should be fetched.'),
    '#default_value' => !empty($form_state['values']['url']) ? $form_state['values']['url'] : 'http://',
  );

  $form['url_pages'] = array(
    '#type' => 'textfield',
    '#title' => t('Page URL regular expression'),
    '#description' => t('If the data you are interested in are (or could be) spread over multiple pages, please enter a regular expression for the page structure and all the found pages will be crawled. For example: http://example.com/\\?page=\\d+'),
    '#default_value' => !empty($form_state['values']['url_pages']) ? $form_state['values']['url_pages'] : ''
  );

  $types = array();
  $node_types = node_type_get_types();
  foreach ($node_types as $name => $type) {
    $types[$name] = $type->name;
  }

  $form['content_type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#description' => t('Choose which content type the data should be saved as. In the next step you will match your wanted data to the corresponding field of the content type.'),
    '#options' => $types,
    '#default_value' => !empty($form_state['values']['content_type']) ? $form_state['values']['content_type'] : ''
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#name' => 'cancel',
    '#submit' => array('webscraper_job_add_form_cancel'),
    '¨#limit_validation_errors' => array()
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and continue') . ' ' . html_entity_decode('&raquo;'),
    '#submit' => array('webscraper_job_add_form_page_1_submit')
  );

  return $form;
}

function webscraper_job_add_form_page_1_validate($form, &$form_state) {
  // Prevent this from running when the "Back" button is pressed in step 2
  if ($form_state['page_num'] == 1 && $form_state['triggering_element']['#name'] != 'cancel') {
    $fetch_function = variable_get('webscraper_fetch_method', 'curl');

    // Try fetching the requested page to see whats on it and if the URL is correct
    try {

      // Fetch the data using the correct fetch function
      $form_state['fetched_data'] = call_user_func(
        '_webscraper_load_site_' . $fetch_function, $form_state['values']['url'],
        $form_state['values']['user_agent_group']
      );

    } catch (Exception $e) {
      form_set_error('url', t('Could not fetch data from the given URL.'));
    }
  }
}

function webscraper_job_add_form_page_1_submit($form, &$form_state) {

  // Values are saved for each page.
  // to carry forward to subsequent pages in the form.
  // and we tell FAPI to rebuild the form.
  $form_state['page_values'][1] = $form_state['values'];

  if (!empty($form_state['page_values'][2])) {
    $form_state['values'] = $form_state['page_values'][2];
  }

  // When form rebuilds, it will look at this to figure which page to build.
  $form_state['page_num'] = 2;
  $form_state['rebuild'] = TRUE;

}

/**
 * Implements hook_form().
 */
function webscraper_job_add_form_page_2($form, &$form_state) {
  $form = array();

  $form['step'] = array(
    '#markup' => '<h2>' . t('Step 2 of 4: Fields') . '</h2>'
  );

  $form['instructions'] = array(
    '#markup' => '<p>' . t('All fields for the chosen content type is shown below. Choose how to identify the data that should be saved to that field.') . '</p>'
  );

  $form['info']['headline'] = array(
    '#markup' => '<h4>' . t('Information about ') . $form_state['page_values'][1]['url'] . '</h4>'
  );

  $form['info']['content_type'] = array(
    '#type' => 'fieldset',
    '#title' => 'Content type',
    '#description' => ($form_state['fetched_data']['content_type'] != "" ? $form_state['fetched_data']['content_type'] : t('Unknown')),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );

  $form['info']['charset'] = array(
    '#type' => 'fieldset',
    '#title' => 'Charset',
    '#description' => ($form_state['fetched_data']['charset'] != "" ? $form_state['fetched_data']['charset'] : t('Unknown')),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );

  $form['info']['header'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header'),
    '#description' => '<div style="max-height:500px;overflow:auto;"><pre>' . htmlentities($form_state['fetched_data']['header']) . '</pre></div>',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE
  );

  $form['info']['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content'),
    '#description' => '<div style="max-height:500px;overflow:auto;"><pre>' . htmlentities($form_state['fetched_data']['body']) . '</pre></div>',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE
  );

  $form['fields'] = array(
    '#markup' => '<h4>' . t('Fields') . '</h4>'
  );

  // Get all fields from the chosen content type
  $fields_info = field_info_instances("node", $form_state['page_values'][1]['content_type']);
  foreach ($fields_info as $field_name => $value) {
    $field_info = field_info_field($field_name);
    $type = $field_info['type'];

    $form[$field_name] = array(
      '#type' => 'fieldset',
      '#title' => $value['label'],
      '#description' => t('Type') . ': <i>' . $type . '</i>',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE
    );

    $form[$field_name]['match_method_' . $field_name] = array(
      '#type' => 'select',
      '#title' => t('Matching method'),
      '#description' => t('Choose what method to use in order to find the wanted data.'),
      '#options' => array('xpath' => t('XPath'), 'regex' => t('Regular expression')),
      '#default_value' => !empty($form_state['values']['match_method_' . $field_name]) ? $form_state['values']['match_method_' . $field_name] : 'xpath'
    );

    $form[$field_name]['match_string_' . $field_name] = array(
      '#type' => 'textfield',
      '#title' => 'String',
      '#description' => t('Type the expression which should be used with the selected method.'),
      '#default_value' => !empty($form_state['values']['match_string_' . $field_name]) ? $form_state['values']['match_string_' . $field_name] : ''
    );

  }

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => html_entity_decode('&laquo;') . ' ' . t('Back'),
    '#submit' => array('webscraper_job_add_form_page_2_back'),
    '#limit_validation_errors' => array(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and continue') . ' ' . html_entity_decode('&raquo;'),
    '#submit' => array('webscraper_job_add_form_page_2_submit')
  );

  return $form;
}

function webscraper_job_add_form_page_2_submit($form, &$form_state) {

  $dom = _webscraper_load_data_into_dom($form_state['fetched_data']);

  $fields_info = field_info_instances("node", $form_state['page_values'][1]['content_type']);
  foreach ($fields_info as $field_name => $value) {

    switch ($form_state['values']['match_method_' . $field_name]) {

      case 'xpath':
        $selected_data = _webscraper_select_data_xpath($dom, $form_state['values']['match_string_' . $field_name]);
      break;

      case 'regex':

      break;

    }

  }

  // Values are saved for each page.
  // to carry forward to subsequent pages in the form.
  // and we tell FAPI to rebuild the form.
  $form_state['page_values'][2] = $form_state['values'];

  if (!empty($form_state['page_values'][3])) {
    $form_state['values'] = $form_state['page_values'][3];
  }

  // When form rebuilds, it will look at this to figure which page to build.
  $form_state['page_num'] = 3;
  $form_state['rebuild'] = TRUE;

}

function webscraper_job_add_form_page_2_back($form, &$form_state) {
  $form_state['values'] = $form_state['page_values'][1];
  $form_state['page_num'] = 1;
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_form().
 */
function webscraper_job_add_form_page_3($form, &$form_state) {
  $form = array();

  $form['step'] = array(
    '#markup' => '<h2>' . t('Step 3 of 4: Conditions and filters') . '</h2>'
  );

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => html_entity_decode('&laquo;') . ' ' . t('Back'),
    '#submit' => array('webscraper_job_add_form_page_3_back'),
    '#limit_validation_errors' => array(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and continue') . ' ' . html_entity_decode('&raquo;'),
    '#submit' => array('webscraper_job_add_form_page_3_submit')
  );

  return $form;
}

function webscraper_job_add_form_page_3_submit($form, &$form_state) {

  // Values are saved for each page.
  // to carry forward to subsequent pages in the form.
  // and we tell FAPI to rebuild the form.
  $form_state['page_values'][3] = $form_state['values'];

  if (!empty($form_state['page_values'][4])) {
    $form_state['values'] = $form_state['page_values'][4];
  }

  // When form rebuilds, it will look at this to figure which page to build.
  $form_state['page_num'] = 4;
  $form_state['rebuild'] = TRUE;

}

function webscraper_job_add_form_page_3_back($form, &$form_state) {
  $form_state['values'] = $form_state['page_values'][2];
  $form_state['page_num'] = 2;
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_form().
 */
function webscraper_job_add_form_page_4($form, &$form_state) {
  $form = array();

  $form['step'] = array(
    '#markup' => '<h2>' . t('Step 4 of 4: Scheduling') . '</h2>'
  );

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => html_entity_decode('&laquo;') . ' ' . t('Back'),
    '#submit' => array('webscraper_job_add_form_page_4_back'),
    '#limit_validation_errors' => array(),
  );

  return $form;
}

function webscraper_job_add_form_page_4_back($form, &$form_state) {
  $form_state['values'] = $form_state['page_values'][3];
  $form_state['page_num'] = 3;
  $form_state['rebuild'] = TRUE;
}
